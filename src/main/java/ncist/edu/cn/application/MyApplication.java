package ncist.edu.cn.application;

import android.app.Application;

import java.util.ArrayList;
import java.util.List;

import cn.bmob.v3.Bmob;
import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.SharedPreferencesUtils;


public class MyApplication extends Application {

    private List<DBWord> words;//保存记忆的单词
    private List<DBWord> nextWords;//保存下一轮询的单词
    private int position;//单词当前的位置
    private int masterWordsCount = 0;//今日已掌握单词书
    private int todayNewWordsCount = 0;//今日新词数
    private int todayWordsCounts = 0;//今日单词数
    private int lastStartWordId = 0;//上次单词的id
    private int todayLearnedWordsCount = 0;//已完成单词数
    private MyUser currentUser;

    public MyUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(MyUser currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * 除非是很多地方需要初始化，否则不要写到这个方法里，因为会拖慢app的启动时间
     */
    @Override
    public void onCreate() {
        super.onCreate();
        // 使用时请将第二个参数Application ID替换成你在Bmob服务器端创建的Application ID
        Bmob.initialize(this, Constant.BMOB_APP_ID);
        words = new ArrayList<>();
        nextWords = new ArrayList<>();
    }

    public List<DBWord> getWords() {
        return words;
    }

    public void setWords(List<DBWord> words) {
        this.words.clear();
        this.words.addAll(words);
    }

    public List<DBWord> getNextWords() {
        return nextWords;
    }

    public void setNextWords(List<DBWord> nextWords) {
        this.nextWords.clear();
        this.nextWords.addAll(nextWords);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getMasterWordsCount() {
        return masterWordsCount;
    }

    public void setMasterWordsCount(int masterWordsCount) {
        this.masterWordsCount = masterWordsCount;
    }

    public int getTodayNewWordsCount() {
        return todayNewWordsCount;
    }

    public void setTodayNewWordsCount(int todayNewWordsCount) {
        this.todayNewWordsCount = todayNewWordsCount;
    }

    public int getTodayWordsCounts() {
        return todayWordsCounts;
    }

    public void setTodayWordsCounts(int todayWordsCounts) {
        this.todayWordsCounts = todayWordsCounts;
    }

    public int getLastStartWordId() {
        return lastStartWordId;
    }

    public void setLastStartWordId(int lastStartWordId) {
        this.lastStartWordId = lastStartWordId;
    }


    public int getTodayLearnedWordsCount() {
        return todayLearnedWordsCount;
    }

    public void setTodayLearnedWordsCount(int todayLearnedWordsCount) {
        this.todayLearnedWordsCount = todayLearnedWordsCount;
    }

    /**
     * 回收内存的时候需要将登录状态改成false
     */
    @Override
    public void onTrimMemory(int level) {
        SharedPreferencesUtils.putBoolean(this,"isLogin",false);
        super.onTrimMemory(level);
    }
}
