package ncist.edu.cn.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

import ncist.edu.cn.R;
import ncist.edu.cn.fragment.WelcomeFragment01;
import ncist.edu.cn.fragment.WelcomeFragment02;
import ncist.edu.cn.fragment.WelcomeFragment03;

public class WelcomeActivity extends FragmentActivity {


    private ViewPager viewPager;
    private FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
        adapter = new InnerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
    }

    //初始化控件
    private void init() {
        viewPager = (ViewPager) findViewById(R.id.welcome_view_pager);
    }

    private class InnerAdapter extends FragmentPagerAdapter{

        public InnerAdapter(FragmentManager fm){
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position){
                case 0:
                    fragment = new WelcomeFragment01();
                    break;
                case 1:
                    fragment = new WelcomeFragment02();
                    break;
                case 2:
                    fragment = new WelcomeFragment03();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
