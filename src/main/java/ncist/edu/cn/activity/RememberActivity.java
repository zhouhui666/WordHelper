package ncist.edu.cn.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import ncist.edu.cn.R;
import ncist.edu.cn.application.MyApplication;
import ncist.edu.cn.dal.DBDao;
import ncist.edu.cn.dal.DBWordDao;
import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.entity.MyLearningRecord;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.entity.MyWord;
import ncist.edu.cn.entity.NewWords;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.DialogUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.utils.ValidatorUtil;
import ncist.edu.cn.view.TitleBar;

public class RememberActivity extends AppCompatActivity implements TitleBar
        .OnIndicatorClickListener {

    @Bind(R.id.remember_titleBar)
    TitleBar titleBar;
    @Bind(R.id.remember_tv_detail_spelling)
    TextView tvSpelling;
    @Bind(R.id.remember_tv_detail_pronounce)
    TextView tvPronounce;
    @Bind(R.id.remember_tv_detail_wordCount)
    TextView tvWordCount;
    @Bind(R.id.remember_tv_detail_meanning)
    TextView tvMeanning;
    @Bind(R.id.remember_btn_detail_know)
    Button btnKnow;
    @Bind(R.id.remember_btn_detail_unknow)
    Button btnUnknow;
    @Bind(R.id.remember_btn_detail_add)
    Button btnAdd;
    @Bind(R.id.remember_btn_detail_ignore)
    Button btnIgnore;

    private DBDao dbDao;
    private DBWordDao dbWordDao;
    private List<DBWord> words;
    private int position = 0;
    private List<DBWord> nextWords;
    private Handler handler;
    private int masterWordsCount = 0;//今日已掌握单词书
    private int todayNewWordsCount = 0;//今日新词数
    private int todayWordsCount = 0;//今日单词数
    private int lastStartWordId = 0;
    private int todayLearnedWordsCount = 0;
    private final int GET_REMEMBER_WORD_SUCCEED = 1;
    private MyApplication app;
    private Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remember);
        ButterKnife.bind(this);
        handler = new Handler(new InnerCallBack());
        dbDao = new DBDao(this);
        dbWordDao = new DBWordDao(this);
        todayWordsCount = SharedPreferencesUtils.getInteger(this, "todayWordsCount", 20);
        app = (MyApplication) getApplicationContext();
        titleBar.setOnIndicatorClickListener(this);
        getApplicationData();
    }

    /**
     * 初始化单词数据
     */
    private void init() {
        BmobQuery<MyLearningRecord> query = new BmobQuery<>();
        String userId = SharedPreferencesUtils.getString(this, "userId");
        query.addWhereEqualTo("userId", userId);
        query.findObjects(this, new FindListener<MyLearningRecord>() {
            @Override
            public void onSuccess(List<MyLearningRecord> list) {//如果存在学习记录,则从lastStartId获取单词
                if (list != null && list.size() > 0) {
                    SharedPreferencesUtils.putInteger(RememberActivity.this, "lastStartWordId",
                            list.get(0).getLastStartWordId());
                    String ids = dbDao.getIdsByWordsAndStartId(todayNewWordsCount, list.get(0)
                            .getLastStartWordId());
                    words.addAll(dbDao.getDBWordsById(ids));
                    lastStartWordId = Integer.parseInt(words.get(words.size() - 1).getId());
                    Collections.shuffle(words);
                    show();
                } else {//如果是新用户,则默认加载数据
                    initNewData();
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error", i + s);
            }
        });
    }

    /**
     * 新用户加载数据
     */
    public void initNewData() {
        String ids = dbDao.getIdsByWordsAndStartId(todayWordsCount, 1);
        words.addAll(dbDao.getDBWordsById(ids));
        Collections.shuffle(words);
        show();

    }

    public void show() {
        DBWord word = words.get(position);
        if (word.getStatus() == null) {
            words.get(position).setStatus(3);
        }
        tvSpelling.setText(word.getSpelling());
        tvPronounce.setText(word.getPhoneticAlphabet());
        tvWordCount.setText(position + 1 + "/" + words.size());
        tvMeanning.setVisibility(View.INVISIBLE);
    }

    @OnClick({R.id.remember_btn_detail_know, R.id.remember_btn_detail_unknow, R.id
            .remember_btn_detail_add, R.id.remember_btn_detail_ignore})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.remember_btn_detail_know:
                next();
                if (words.size() != 0) {
                    show();
                }
                break;
            case R.id.remember_btn_detail_unknow:
                unKnow();
                break;
            case R.id.remember_btn_detail_add:
                add();
                break;
            case R.id.remember_btn_detail_ignore:
                ignore();
                break;
        }
    }

    public void next() {
        if(todayLearnedWordsCount < todayWordsCount) {
            todayLearnedWordsCount++;
        }
        int status = words.get(position).getStatus() - 1;//认识+1
        words.get(position).setStatus(status);
        if (status != 0) {
            uploadMyWord(words.get(position));//上传上个单词的学习记录
        }
        if (position == words.size() - 1) {//看是否要进入下一轮
            words.clear();
            words.addAll(nextWords);
            position = -1;//初始化起始位置
            if (words.size() == 0) {//说明已经学习完毕
                position = 0;
                upload();
                return;
            }
            nextWords.clear();//进入下一轮训数组
        }
        position++;
        if (status == 0) {
            masterWordsCount++;//已经掌握的词数
        }
    }

    public void unKnow() {
        DBWord word = words.get(position);
        if (!nextWords.contains(word)) {
            word.setStatus(word.getStatus() + 3);
            nextWords.add(word);
        }
        tvMeanning.setVisibility(View.VISIBLE);
        tvMeanning.setText(word.getMeaning());
        showBtnKnow(false);
        uploadMyWord(word);
    }

    public void add() {
        DBWord word = words.get(position);
        //dbWordDao.add(word);
        //应该是加入云端数据库
        NewWords newWords = new NewWords();
        MyUser user = app.getCurrentUser();
        newWords.setSpelling(word.getSpelling());
        newWords.setMeanning(word.getMeaning());
        if (ValidatorUtil.isNotEmpty(user.getMobile())) {
            newWords.setMobile(user.getMobile());
        }
        if (ValidatorUtil.isNotEmpty(user.getUserId())) {
            newWords.setUserId(user.getUserId());
        }
        newWords.save(this, new SaveListener() {
            @Override
            public void onSuccess() {
                CommonUtils.showToast(RememberActivity.this, "加入生词本成功!");
                showBtnKnow(true);
                next();
                show();
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(RememberActivity.this, "添加失败!");
                Log.e("error", i + s);
            }
        });
    }

    private void showBtnKnow(boolean flag) {
        if (flag) {
            btnIgnore.setVisibility(View.INVISIBLE);
            btnAdd.setVisibility(View.INVISIBLE);
            btnKnow.setVisibility(View.VISIBLE);
            btnUnknow.setVisibility(View.VISIBLE);
        } else {
            btnKnow.setVisibility(View.INVISIBLE);
            btnUnknow.setVisibility(View.INVISIBLE);
            btnAdd.setVisibility(View.VISIBLE);
            btnIgnore.setVisibility(View.VISIBLE);
        }
    }

    public void ignore() {
        showBtnKnow(true);
        next();
        show();
    }

    public void upload() {
        final MyLearningRecord record = new MyLearningRecord();
        final String userId = SharedPreferencesUtils.getString(this, "userId");
        record.setMobile(userId);
        BmobQuery<MyLearningRecord> query = new BmobQuery<>();
        query.addWhereEqualTo("userId", userId);
        query.findObjects(this, new FindListener<MyLearningRecord>() {
            @Override
            public void onSuccess(List<MyLearningRecord> list) {
                final MyLearningRecord result;
                if (list != null && list.size() > 0) {//说明存在学习记录
                    result = list.get(0);
                    result.setDays(result.getDays() + 1);//累计天数加1
                    // 新增掌握的词汇量
                    result.setMasteredWords(result.getMasteredWords() + masterWordsCount);
                    // 新增累计的词汇量
                    result.setLearnedWords(result.getLearnedWords() + todayNewWordsCount);
                    result.setTodayWords(todayWordsCount);//今日单词数
                    result.setTodayLearnedWords(todayWordsCount);//今日已完成单词数
                    result.setLastStartWordId(lastStartWordId);//最后一个单词的id
                    result.update(RememberActivity.this, new UpdateListener() {
                        @Override
                        public void onSuccess() {
                            CommonUtils.showToast(RememberActivity.this, "数据更新成功!");
                            Intent intent = new Intent();
                            intent.putExtra("record", result);
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            CommonUtils.showToast(RememberActivity.this, "数据更新失败!");
                        }
                    });
                } else {//没有学习记录则添加学习记录
                    result = new MyLearningRecord();
                    MyUser user = app.getCurrentUser();
                    result.setUserId(user.getUserId());
                    if (user.getMobile() != null) {
                        result.setMobile(user.getMobile());
                    }
                    result.setDays(1);//累计天数
                    result.setLearnedWords(todayWordsCount);//累计学习单词数
                    result.setMasteredWords(masterWordsCount);//累计掌握单词数
                    result.setTodayWords(todayWordsCount);//今日单词数
                    result.setTodayNewWords(result.getTodayWords());// 新词数
                    result.setTodayLearnedWords(todayLearnedWordsCount);//已学习单词数
                    result.setLastStartWordId(lastStartWordId);//最后一个单词的id
                    result.save(RememberActivity.this, new SaveListener() {
                        @Override
                        public void onSuccess() {
                            CommonUtils.showToast(RememberActivity.this, "数据上传成功!");
                            Intent intent = new Intent();
                            intent.putExtra("record", result);
                            setResult(RESULT_OK, intent);
                            finish();
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            CommonUtils.showToast(RememberActivity.this, "数据上传失败!");
                        }
                    });
                }
            }

            @Override
            public void onError(int i, String s) {
                CommonUtils.showToast(RememberActivity.this, "查询失败!");
            }
        });

    }

    /**
     * 上次单词学习记录
     */
    public void uploadMyWord(DBWord word) {
        final MyWord myWord = new MyWord();
        myWord.setUserid(SharedPreferencesUtils.getString(this, "userId"));
//        myWord.setMobile(SharedPreferencesUtils.getString(this, "mobile"));
        myWord.setId(word.getId());
        myWord.setStatus(word.getStatus());
        BmobQuery<MyWord> query = new BmobQuery<>();
        query.addWhereEqualTo("id", word.getId());
        query.findObjects(this, new FindListener<MyWord>() {
            @Override
            public void onSuccess(List<MyWord> list) {
                if (list != null && list.size() > 0) {//之前存在记录
                    myWord.update(RememberActivity.this, list.get(0).getObjectId(), new
                            UpdateListener() {
                        @Override
                        public void onSuccess() {
                            Log.d("debug", myWord.getId() + "更新成功");
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            Log.e("error", myWord.getId() + ":更新失败->" + s);
                        }
                    });
                } else {//说明之前没有记录
                    myWord.save(RememberActivity.this, new SaveListener() {
                        @Override
                        public void onSuccess() {
                            Log.d("debug", myWord.getId() + "添加成功");
                        }

                        @Override
                        public void onFailure(int i, String s) {
                            Log.e("error", myWord.getId() + ":添加失败->" + s);
                        }
                    });
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error", "MyWord查找失败");
            }
        });
    }

    /**
     * 查询复习单词的id
     */
    public void getRememberWordId() {
        //先查询学习单词记录是否存在，如果存在，查询复习的单词
        BmobQuery<MyLearningRecord> record = new BmobQuery<>();
        record.addWhereEqualTo("userId", app.getCurrentUser().getUserId());
        record.findObjects(this, new FindListener<MyLearningRecord>() {
            @Override
            public void onSuccess(List<MyLearningRecord> list) {
                if (list != null && list.size() > 0) {
                    //存在学习记录则查找复习单词
                    BmobQuery<MyWord> query = new BmobQuery<>();
                    query.addWhereEqualTo("userId", app.getCurrentUser().getUserId());
                    query.findObjects(RememberActivity.this, new FindListener<MyWord>() {
                        @Override
                        public void onSuccess(List<MyWord> list) {
                            hideDialog();
                            if (list != null && list.size() > 0) {
                                Message.obtain(handler, GET_REMEMBER_WORD_SUCCEED, list)
                                        .sendToTarget();
                            } else {
                                init();
                            }
                        }

                        @Override
                        public void onError(int i, String s) {
                            Log.e("error", i + s);
                        }
                    });
                } else {
                    //如果不存在，初始化数据
                    initNewData();
                    hideDialog();
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error", i + s);
            }
        });
    }

    private class InnerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case GET_REMEMBER_WORD_SUCCEED:
                    List<MyWord> myWords = (List<MyWord>) msg.obj;
                    //得到从云端加载的单词id
                    String ids = getMyWordsIds(myWords);
                    //根据单词id得到复习单词
                    words = dbDao.getDBWordsById(ids);
                    for (int i = 0; i < words.size(); i++) {
                        for (int j = 0; j < myWords.size(); j++) {
                            if (myWords.get(j).getId().equals(words.get(i).getId())) {
                                words.get(i).setStatus(myWords.get(j).getStatus());
                                break;
                            }
                        }
                    }
                    //今日的新单词数量
                    todayNewWordsCount = todayWordsCount - words.size();
                    init();
                    break;
            }
            return false;
        }
    }

    public String getMyWordsIds(List<MyWord> myWords) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < myWords.size(); i++) {
            builder.append(myWords.get(i).getId());
            if (i != myWords.size() - 1) {
                builder.append(",");
            }
        }
        return builder.toString();
    }

    /**
     * 监听返回操作
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        isComplete();
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 判断是否学习完毕
     */
    public void isComplete() {
        if (position == words.size() - 1) {//看是否要进入下一轮
            words.clear();
            words.addAll(nextWords);
            position = -1;//初始化起始位置
            if (words.size() == 0) {//说明已经学习完毕
                position = 0;
                upload();
                clearApplication();//清除Application的数据
            }
        } else {
            CommonUtils.showAlertDialog(this, "提醒", "您还没有学习完毕,确认要退出吗?", new DialogInterface
                    .OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //若没有,提示是否返回,若返回,保存当前的轮训单词状态和位置
                    setApplicationData();
                    Intent intent = new Intent();
                    intent.putExtra("todayLearnedWordsCount", todayLearnedWordsCount);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }, null);
        }
    }

    /**
     * 保存当前进度
     */
    public void setApplicationData() {
        app.setPosition(position);
        app.setWords(words);
        app.setNextWords(nextWords);
        app.setMasterWordsCount(masterWordsCount);
        app.setLastStartWordId(lastStartWordId);
        app.setTodayNewWordsCount(todayNewWordsCount);
        app.setTodayWordsCounts(todayWordsCount);
        app.setTodayLearnedWordsCount(todayLearnedWordsCount);
    }

    /**
     * 读取Appllication的数据
     */
    public void getApplicationData() {
        words = new ArrayList<>();
        nextWords = new ArrayList<>();
        words.addAll(app.getWords());
        nextWords.addAll(app.getNextWords());
        if (words.size() == 0 && nextWords.size() == 0) {
            dialog = DialogUtils.createLoadingDialog(this, "数据加载中...");
            getRememberWordId();//获取上次要复习的单词数
        } else {
            //这种情况说明是同一个用户
            position = app.getPosition();
            masterWordsCount = app.getMasterWordsCount();
            lastStartWordId = app.getLastStartWordId();
            todayNewWordsCount = app.getTodayNewWordsCount();
            todayWordsCount = app.getTodayWordsCounts();
            todayLearnedWordsCount = app.getTodayLearnedWordsCount();
            show();
        }
    }

    /**
     * 清除Application的数据
     */
    public void clearApplication() {
        words.clear();
        nextWords.clear();
        app.setPosition(0);
        app.setWords(words);
        app.setNextWords(nextWords);
        app.setMasterWordsCount(0);
        app.setLastStartWordId(0);
        app.setTodayNewWordsCount(0);
        app.setTodayWordsCounts(0);
        app.setTodayLearnedWordsCount(0);
    }

    public void hideDialog() {
        if (dialog != null) {
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    protected void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }

    @Override
    public void onIndicatorClick() {
        isComplete();
    }
}
