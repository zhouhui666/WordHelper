package ncist.edu.cn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ncist.edu.cn.R;
import ncist.edu.cn.view.TitleBar;

public class SettingActivity extends AppCompatActivity implements TitleBar
        .OnIndicatorClickListener {


    private static final int SETTING_SUCCEED = 1;
    @Bind(R.id.setting_titlebar)
    TitleBar settingTitlebar;
    @Bind(R.id.setting_tv_words_count)
    TextView tvWordsCount;
    @Bind(R.id.setting_tv_count)
    TextView settingTvCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        settingTitlebar.setOnIndicatorClickListener(this);
    }

    @OnClick(R.id.setting_tv_words_count)
    public void onClick() {
        Intent intent = new Intent(this, SelectWordsCountActivity.class);
        startActivityForResult(intent, SETTING_SUCCEED);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case SETTING_SUCCEED:
                    int todayWordsCount = data.getIntExtra("todayWordsCount", 0);
                    if (todayWordsCount != 0) {
                        settingTvCount.setText(todayWordsCount + "");
                    }
                    break;
            }
        }
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }
}
