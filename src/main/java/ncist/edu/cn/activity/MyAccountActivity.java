package ncist.edu.cn.activity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.datatype.BmobFile;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.bmob.v3.listener.UploadFileListener;
import ncist.edu.cn.R;
import ncist.edu.cn.application.MyApplication;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.DialogUtils;
import ncist.edu.cn.utils.FilePathUtils;
import ncist.edu.cn.utils.ImageUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.utils.ValidatorUtil;
import ncist.edu.cn.view.TitleBar;

import static ncist.edu.cn.dal.BmobDao.queryByMap;

public class MyAccountActivity extends AppCompatActivity implements TitleBar
        .OnIndicatorClickListener {

    private static final int UPDATE_USERNAME = 1;
    @Bind(R.id.my_account_titlebar)
    TitleBar myAccountTitlebar;
    @Bind(R.id.setting_tv_words_count)
    TextView myAccountTvUsername;
    @Bind(R.id.my_account_tv_mobile)
    TextView myAccountTvMobile;
    @Bind(R.id.my_account_image)
    SimpleDraweeView myAccountImage;

    /**
     * 保存用相机拍摄的准备用来充当头像照片的原始照片路径
     */
    private String filePath;


    /**
     * 保存经过剪裁后正式用于头像的照片路径
     */
    private String avatarCameraPath;

    /**
     * 部分手机，在拍摄照片后，2次打开的时候准备剪裁时会将照片旋转90度
     * 该属性就是用来读取是否有将拍摄的原始照片进行旋转
     * 如果发生了旋转，在2次打开进行裁剪的时候，必须反向旋转回来
     */
    private int degree;

    /**
     * 头像照片是来自相机拍照还是图库选择
     */
    private boolean isCamera;

    private Dialog dialog;

    private MyApplication app;

    private Handler handler;
    private final int MSEEAGE_USER_ICON_NOT_NULL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_my_account);
        ButterKnife.bind(this);
        myAccountTitlebar.setOnIndicatorClickListener(this);
        app = (MyApplication) getApplication();
        handler = new Handler(new InnerCallback());
        initData();
    }

    /**
     * 初始化数据
     */
    public void initData() {
        //初始化头像和用户名
        if (ValidatorUtil.isNotEmpty(app.getCurrentUser().getIcon())) {
            myAccountImage.setImageURI(Uri.parse(app.getCurrentUser().getIcon()));
            if (ValidatorUtil.isNotEmpty(app.getCurrentUser().getUsername())) {
                myAccountTvUsername.setText(app.getCurrentUser().getUsername());
            }
        }
        //如果不是第三方登录，则userId为空，初始化手机号
        if (ValidatorUtil.isEmpty(app.getCurrentUser().getUserId())) {
            Map<String, Object> map = new HashMap<>();
            String mobile = SharedPreferencesUtils.getString(this, "mobile");
            map.put("mobile", mobile);
            queryByMap(this, map, MyUser.class, new FindListener<MyUser>() {
                @Override
                public void onSuccess(List<MyUser> list) {
                    if (list != null && list.size() > 0) {
                        MyUser user = list.get(0);
                        String username = user.getUsername();
                        myAccountTvUsername.setText(username);
                        String mobile = Constant.MOBILE;
                        if (ValidatorUtil.isNotEmpty(mobile)) {
                            mobile = mobile.substring(0, 3) + "****" + mobile.substring(7, mobile
                                    .length());
                            myAccountTvMobile.setText(mobile);
                        }
                        if (ValidatorUtil.isNotEmpty(user.getIcon())) {
                            Message.obtain(handler, MSEEAGE_USER_ICON_NOT_NULL, user.getIcon())
                                    .sendToTarget();
                        }
                    }
                }

                @Override
                public void onError(int i, String s) {
                    Log.e("error",i+s);
                }
            });
        }
    }

    private class InnerCallback implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSEEAGE_USER_ICON_NOT_NULL:
                    String avatar = (String) msg.obj;
                    myAccountImage.setImageURI(Uri.parse(avatar));
                    break;
            }
            return false;
        }
    }

    @OnClick({R.id.setting_tv_words_count, R.id.my_account_tv_modify_password})
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.setting_tv_words_count:
                intent = new Intent(this, SettingUsernameActivity.class);
                startActivityForResult(intent, UPDATE_USERNAME);
                break;
            case R.id.my_account_tv_modify_password:
                intent = new Intent(this, FindPasswordActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case UPDATE_USERNAME:
                    String username = data.getStringExtra("username");
                    if (username != null) {
                        myAccountTvUsername.setText(username);
                    }
                    break;
                case Constant.REQUESTCODE_UPLOADAVATAR_CROP:
                    if (data == null) {
                        return;
                    } else {
                        //保存剪裁完毕的头像并显示
                        saveCropAvatar(data);
                    }
                    filePath = "";
                    // 删除Bmob服务器上的头像
                    deleteAvatar();
                    // 将头像上传到Bmob服务器并更新用户的数据
                    uploadAvatar();
                    break;
                case Constant.REQUESTCODE_UPLOADAVATAR_CAMERA_OR_MEDIASTORE:
                    //如果data为空说明来自相机拍照返回的结果，如果不为空则是图库选择返回的结果
                    if (data == null) {
                        isCamera = true;
                        File file = new File(filePath);
                        //如果图像确实被旋转了，必须在2次剪裁编辑前将其旋转回来
                        degree = ImageUtils.readPictureDegree(filePath);
                        if (degree != 0) {
                            //以压缩的方式获取已经被旋转后保存的图像
                            Bitmap bitmap = ImageUtils.getSampleSizeBitmap(filePath,
                                    MyAccountActivity
                                            .this);
                            //将图像旋转回拍摄时的角度，并保存回原位置
                            ImageUtils.saveBitmap(file, ImageUtils.rotaingImageView(degree,
                                    bitmap), true);
                        }
                        //准备对刚才拍摄的头像进行剪裁
                        setImageCrop(Uri.fromFile(file), 200, 200, Constant
                                .REQUESTCODE_UPLOADAVATAR_CROP);

                    } else {
                        Uri uri = null;
                        isCamera = false;
                        uri = data.getData();
                        //准备对刚才图库选择的头像进行剪裁
                        setImageCrop(uri, 200, 200, Constant.REQUESTCODE_UPLOADAVATAR_CROP);
                    }
                    break;
            }
        }
    }

    private void deleteAvatar() {
        MyUser user = app.getCurrentUser();
        BmobFile file = new BmobFile();
        file.setUrl(user.getIcon());
        file.delete(this, new DeleteListener() {
            @Override
            public void onSuccess() {
                Log.d("debug", "删除头像成功!");
            }

            @Override
            public void onFailure(int i, String s) {
                Log.d("error", "删除头像失败!");
            }
        });
    }

    /**
     * 进行图像剪裁的必要设置，具体参数可以参考Google相关文档
     * 剪裁完毕后，依然会将结果送入onActivityResult方法中
     */
    private void setImageCrop(Uri uri, int outputX, int outputY, int requestCode) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", outputX);
        intent.putExtra("outputY", outputY);
        intent.putExtra("scale", true);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.putExtra("return-data", true);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        startActivityForResult(intent, requestCode);
    }

    /**
     * 将设置好的头像上传到Bmob服务器上
     * 该方法做两件事情：
     * 1）先上传头像图像本身到服务器，上传成功后获得该图像在服务器上的地址，将地址作为BmobChatUser的avatar属性的值
     * 2）新建要给BmobChatUser，设置它的objectID和avatar属性的值，并上传到服务器，服务器会根据objectID更新用户表中对应用户的头像文件地址
     */
    private void uploadAvatar() {
        /**利用图像文件的路径，创建一个BmobFile类型的文件，以上传到Bmob服务器*/
        final BmobFile bmobFile = new BmobFile(new File(avatarCameraPath));
        dialog = DialogUtils.createLoadingDialog(this, "头像上传中...");
        bmobFile.uploadblock(this, new UploadFileListener() {
            @Override
            public void onSuccess() {
                hideDialog();
                //上传成功以后，可以获得该文件存储在服务器上的路径
                final String url = bmobFile.getFileUrl(MyAccountActivity.this);
                //接下来要更新当前登录用户的头像地址，将其设置为刚刚上传的图像文件
                MyUser user = app.getCurrentUser();
                user.setIcon(url);
                user.update(MyAccountActivity.this, user.getObjectId(), new UpdateListener() {
                    @Override
                    public void onSuccess() {
                        CommonUtils.showToast(MyAccountActivity.this, "头像更新成功!");
                    }

                    @Override
                    public void onFailure(int i, String s) {
                        Log.e("error", i + s);
                    }
                });
            }

            @Override
            public void onProgress(Integer value) {
                Log.d("debug", "prigress:" + value);
            }

            @Override
            public void onFailure(int i, String s) {
                hideDialog();
                Log.e("error", i + s);
            }
        });
    }

    /**
     * 将剪裁后的正式作为头像使用的图片保存到SD卡上，并在ivAvatar中显示
     *
     * @param data 含有剪裁后图像信息的intent对象
     */
    private void saveCropAvatar(Intent data) {
        Bundle extras = data.getExtras();
        if (extras != null) {
            Bitmap bitmap = extras.getParcelable("data");
            if (bitmap != null) {
                // 将剪裁过的头像图片进行保存
                String filename = "avatar_" + new SimpleDateFormat("yyMMddHHmmss", Locale.CHINA)
                        .format(new
                                Date()) + ".jpg";
                avatarCameraPath = FilePathUtils.getFilePath(this, Constant.MY_AVATAR_PATH) +
                        filename;
                ImageUtils.saveBitmap(FilePathUtils.getFilePath(this, Constant.MY_AVATAR_PATH),
                        filename, bitmap, 20, true);
                myAccountImage.setImageURI(Uri.parse("file://" + avatarCameraPath));
            }
        }
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }

    /**
     * 点击了头像
     */
    @OnClick(R.id.my_account_image)
    public void onClick() {
        //保存相机拍摄照片的本地目录
        String path = FilePathUtils.getFilePath(this, Constant.MY_AVATAR_PATH);
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        // 用相机拍摄的原图文件
        File file = new File(dir, "raw_" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date
                ()));
        filePath = file.getAbsolutePath();
        //创建打开安卓自带相机引用的intent（Intent中需要一个Uri格式表示的相片完整保存）
        Uri imageUri = Uri.fromFile(file);
        Intent takephoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takephoto.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        //创建从图库选择图片的intent
        Intent pickimage = new Intent(Intent.ACTION_PICK);
        pickimage.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        //以某一个intent为主intent创建chooser
        Intent chooser = Intent.createChooser(pickimage, "选择头像...");

        //在该chooser上继续添加intent，这样发送chooser的时候，就会以复合intent的形式发送
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takephoto});

        //发送复合intent，屏幕上会显示可供选择的应用
        startActivityForResult(chooser, Constant.REQUESTCODE_UPLOADAVATAR_CAMERA_OR_MEDIASTORE);
    }

    /**
     * 隐藏对话框
     */
    public void hideDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }
}
