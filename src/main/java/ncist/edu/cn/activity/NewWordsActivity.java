package ncist.edu.cn.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.bmob.v3.BmobObject;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import ncist.edu.cn.R;
import ncist.edu.cn.adapter.NewWordAdapter;
import ncist.edu.cn.application.MyApplication;
import ncist.edu.cn.dal.BmobDao;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.entity.NewWords;
import ncist.edu.cn.utils.DialogUtils;
import ncist.edu.cn.utils.ValidatorUtil;
import ncist.edu.cn.view.TitleBar;


public class NewWordsActivity extends AppCompatActivity implements TitleBar
        .OnIndicatorClickListener {

    private static final int MENU_DELETE = 1;
    private static final int MENU_DELETE_ALL = 2;
    @Bind(R.id.new_words_title)
    TitleBar newWordsTitle;
    @Bind(R.id.new_words_listview)
    ListView listView;

    private List<NewWords> words;
    private NewWordAdapter adapter;
    private MyApplication app;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_words);
        ButterKnife.bind(this);
        words = new ArrayList<>();
        adapter = new NewWordAdapter(this, words);
        listView.setAdapter(adapter);
        newWordsTitle.setOnIndicatorClickListener(this);
        app = (MyApplication) getApplication();
        refresh();
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo
            menuInfo) {
        menu.add(1, MENU_DELETE, 1, "删除");
        menu.add(1, MENU_DELETE_ALL, 2, "全部删除");
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        int position = info.position;
        switch (item.getItemId()) {
            case MENU_DELETE:
                List<NewWords> words = new ArrayList<>();
                words.add(this.words.get(position));
                dialog = DialogUtils.createLoadingDialog(this,"删除中...");
                deleteData(words);
                break;
            case MENU_DELETE_ALL:
                deleteData(this.words);
                break;
        }
        return super.onContextItemSelected(item);
    }

    private void refresh() {
        initData();
    }

    private void initData() {
        MyUser user = app.getCurrentUser();
        Map<String, Object> map = new HashMap<>();
        if (ValidatorUtil.isNotEmpty(user.getMobile())) {
            map.put("mobile", user.getMobile());
        }
        if (ValidatorUtil.isNotEmpty(user.getUserId())) {
            map.put("userId", user.getUserId());
        }
        BmobDao.queryByMap(NewWordsActivity.this, map, NewWords.class, new
                FindListener<NewWords>() {
                    @Override
                    public void onSuccess(List<NewWords> list) {
                        words.clear();
                        words.addAll(list);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(int i, String s) {
                        Log.e("error", i + s);
                    }
                });
    }

    private void deleteData(List<NewWords> newWords) {
        List<BmobObject> objects = new ArrayList<>();
        objects.addAll(newWords);
        new NewWords().deleteBatch(NewWordsActivity.this, objects, new DeleteListener() {
            @Override
            public void onSuccess() {
                dialog.dismiss();
                dialog = null;
                refresh();
                Log.d("debug", "删除生词成功!");
            }

            @Override
            public void onFailure(int i, String s) {
                Log.e("error", "删除生词失败!");
            }
        });
    }

    @Override
    protected void onDestroy() {
        unregisterForContextMenu(listView);
        super.onDestroy();
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }
}
