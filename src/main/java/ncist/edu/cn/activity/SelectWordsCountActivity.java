package ncist.edu.cn.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ncist.edu.cn.R;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.view.TitleBar;

public class SelectWordsCountActivity extends AppCompatActivity implements TitleBar.OnIndicatorClickListener {

    @Bind(R.id.select_words_titlebar)
    TitleBar selectWordsTitlebar;
    @Bind(R.id.select_words_tv_three)
    TextView selectWordsTvThree;
    @Bind(R.id.select_words_tv_five)
    TextView selectWordsTvFive;
    @Bind(R.id.select_words_tv_ten)
    TextView selectWordsTvTen;

    private int todayWordsCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_words_count);
        ButterKnife.bind(this);
        selectWordsTitlebar.setOnIndicatorClickListener(this);
    }

    @OnClick({R.id.select_words_tv_three, R.id.select_words_tv_five, R.id.select_words_tv_ten, R
            .id.select_words_btn_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_words_tv_three:
                selectWordsTvThree.setBackgroundResource(R.color.green);
                selectWordsTvFive.setBackgroundResource(R.color.white);
                selectWordsTvTen.setBackgroundResource(R.color.white);
                todayWordsCount = 30;
                break;
            case R.id.select_words_tv_five:
                selectWordsTvThree.setBackgroundResource(R.color.white);
                selectWordsTvFive.setBackgroundResource(R.color.green);
                selectWordsTvTen.setBackgroundResource(R.color.white);
                todayWordsCount = 50;
                break;
            case R.id.select_words_tv_ten:
                selectWordsTvThree.setBackgroundResource(R.color.white);
                selectWordsTvFive.setBackgroundResource(R.color.white);
                selectWordsTvTen.setBackgroundResource(R.color.green);
                todayWordsCount = 100;
                break;
            case R.id.select_words_btn_submit:
                SharedPreferencesUtils.putInteger(this,"todayWordsCount",todayWordsCount);
                Intent intent = new Intent();
                intent.putExtra("todayWordsCount",todayWordsCount);
                setResult(RESULT_OK,intent);
                finish();
                break;
        }
    }

    @Override
    public void onIndicatorClick() {
        finish();
    }
}
