package ncist.edu.cn.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.MyUser;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.MD5Utils;
import ncist.edu.cn.view.CircleImageView;
import ncist.edu.cn.view.SoftKeyBoardStatusView;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,
        SoftKeyBoardStatusView.SoftKeyBoardListener {


    @Bind(R.id.register_iv_logo)
    CircleImageView registerIvLogo;

    private Button btnRegister;
    private SoftKeyBoardStatusView softKeyBoardStatusView;
    private LinearLayout linearLayout;
    //屏幕滚动距离
    private int scroll_dx;
    private EditText etMobile;
    private EditText etCode;
    private EditText etPassword;
    private Button btnGetCode;
    private EventHandler eventHandler;
    private Handler handler;
    private final static int MSG_MOBILE_EXISTED = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // 初始化Mob短信功能
        SMSSDK.initSDK(this, Constant.MOB_APP_KEY, Constant.MOB_SECRET);
        ButterKnife.bind(this);
        init();
        handler = new Handler(new InnerCallBack());
        eventHandler = new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {
                if (result == SMSSDK.RESULT_COMPLETE) {
                    //回调完成
                    if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {
                        //提交验证码成功
                        Message.obtain(handler, SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE)
                                .sendToTarget();
                    } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                        //获取验证码成功
                        Message.obtain(handler, SMSSDK.EVENT_GET_VERIFICATION_CODE).sendToTarget();
                    } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {
                        //返回支持发送验证码的国家列表
                    }
                } else if(result == SMSSDK.RESULT_ERROR){
                    Throwable throwable = (Throwable) data;
                    throwable.printStackTrace();
                    JSONObject object = null;
                    try {
                        object = new JSONObject(throwable.getMessage());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String des = object.optString("detail");//错误描述
                    int status = object.optInt("status");//错误代码
                    if (status > 0 && !TextUtils.isEmpty(des)) {
                        CommonUtils.showToast(RegisterActivity.this, des);
                        return;
                    }
                }
            }
        };
        SMSSDK.registerEventHandler(eventHandler); //注册短信回调
    }

    @OnClick(R.id.register_tv_login)
    public void onClick() {
        finish();
    }

    private class InnerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                //获取验证码成功
                case SMSSDK.EVENT_GET_VERIFICATION_CODE:
                    Log.d("debug","获取短信验证码成功!");
                    break;
                //验证码验证成功
                case SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE:
                    Log.d("debug","短信验证成功!");
                    break;
                case MSG_MOBILE_EXISTED:

            }
            return false;
        }
    }

    //初始化控件
    private void init() {
        btnRegister = (Button) findViewById(R.id.register_btn_register);
        btnRegister.setOnClickListener(this);
        softKeyBoardStatusView = (SoftKeyBoardStatusView) findViewById(R.id.register_skbsv);
        softKeyBoardStatusView.setSoftKeyBoardListener(this);
        linearLayout = (LinearLayout) findViewById(R.id.register_ll);
        etMobile = (EditText) findViewById(R.id.register_et_mobile);
        etCode = (EditText) findViewById(R.id.register_et_code);
        etPassword = (EditText) findViewById(R.id.register_et_password);
        btnGetCode = (Button) findViewById(R.id.register_btn_get_code);
        etMobile.setBackgroundResource(R.color.smallLightBlack);
        etCode.setBackgroundResource(R.color.smallLightBlack);
        etPassword.setBackgroundResource(R.color.smallLightBlack);
        btnGetCode.setOnClickListener(this);
    }

    //校验手机号,密码输入合法性
    public boolean checkRegisterInput(String mobile, String password) {
        boolean isLegal = true;
        if(mobile.length() == 0){
            isLegal = false;
            Toast.makeText(RegisterActivity.this,"手机号不能为空!",Toast.LENGTH_SHORT).show();
        }
        else if (mobile.length() != 11) {
            isLegal = false;
            Toast.makeText(RegisterActivity.this, "手机号长度必须是11位!", Toast.LENGTH_SHORT).show();
        }
        if (password.length() < 6) {
            isLegal = false;
            Toast.makeText(RegisterActivity.this, "密码长度小于6位!", Toast.LENGTH_SHORT).show();
        }
        return isLegal;
    }

    //检查手机号是否被注册
    public void checkMobileIsAvailable(final String mobile) {
        BmobQuery<MyUser> query = new BmobQuery<>();
        query.addWhereEqualTo("mobile", mobile);
        query.findObjects(this, new FindListener<MyUser>() {
            @Override
            public void onSuccess(List<MyUser> list) {
                if (list != null && list.size() > 0) {
                    CommonUtils.showToast(RegisterActivity.this, "该手机号已经注册!");
                } else {
                    SMSSDK.getVerificationCode("86", mobile);
                    CountDownTime();
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error",i+s);
                switch (i){
                    case 9010:
                        CommonUtils.showToast(RegisterActivity.this,"网络超时!");
                        break;
                    case 9016:
                        CommonUtils.showToast(RegisterActivity.this,"无网络连接，请检查网络设置!");
                        break;
                }
            }
        });

    }

    //设置一个定时器
    public void CountDownTime() {
        btnGetCode.setClickable(false);
        new CountDownTimer(60000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                btnGetCode.setText("剩余:" + millisUntilFinished / 1000 + "秒");
            }

            @Override
            public void onFinish() {
                btnGetCode.setText("重新获取验证码");
                btnGetCode.setClickable(true);
            }
        }.start();
    }

    //注册,用户id为MD5加密后生成的32位的字符串，手机号唯一能确保用户id唯一
    public void register(Context context, final String mobile, final String password) {
        MyUser user = new MyUser();
        String userId = MD5Utils.MD5(mobile);
        user.setUserId(userId);
        user.setMobile(mobile);
        user.setPassword(password);
        user.save(context, new SaveListener() {
            @Override
            public void onSuccess() {
                CommonUtils.showToast(RegisterActivity.this, "注册成功!");
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                intent.putExtra("mobile",mobile);
                intent.putExtra("password",password);
                startActivity(intent);
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(int i, String s) {
                CommonUtils.showToast(RegisterActivity.this, s);
            }
        });
    }

    @Override
    public void onClick(View v) {
        String mobile = etMobile.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        switch (v.getId()) {
            //注册
            case R.id.register_btn_register:
                String code = etCode.getText().toString().trim();
                checkRegisterInput(mobile, password);
                SMSSDK.submitVerificationCode("86", mobile, code);
                register(this, mobile, password);
                break;
            //获取验证码
            case R.id.register_btn_get_code:
                if (checkRegisterInput(mobile, "123456")) {
                    checkMobileIsAvailable(mobile);
                }
                break;
        }
    }

    @Override
    public void keyBoardVisible(int move) {
        registerIvLogo.setVisibility(View.INVISIBLE);
        int[] location = new int[2];
        btnRegister.getLocationOnScreen(location);
        int btnToBottom = Constant.SCREEN_HEIGHT - location[1] - btnRegister.getHeight() * 5 / 4;
        scroll_dx = btnToBottom > move ? 0 : move - btnToBottom;
        linearLayout.scrollBy(0, scroll_dx);
    }

    @Override
    public void keyBoardInvisible(int move) {
        registerIvLogo.setVisibility(View.VISIBLE);
        linearLayout.scrollBy(0, -scroll_dx);
    }

    @Override
    public void keyBoardStatus(int w, int h, int oldw, int oldh) {

    }

    @Override
    protected void onDestroy() {
        if (eventHandler != null) {
            SMSSDK.unregisterEventHandler(eventHandler);
        }
        super.onDestroy();
    }
}
