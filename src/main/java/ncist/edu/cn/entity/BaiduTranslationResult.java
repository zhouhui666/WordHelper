package ncist.edu.cn.entity;

import java.util.List;

public class BaiduTranslationResult {

    private String from;//翻译源语言

    private String to;//译文语言

    public class Trans_result {
        private String src;

        private String dst;

        public void setSrc(String src) {
            this.src = src;
        }

        public String getSrc() {
            return this.src;
        }

        public void setDst(String dst) {
            this.dst = dst;
        }

        public String getDst() {
            return this.dst;
        }

        @Override
        public String toString() {
            return "Trans_result{" +
                    "src='" + src + '\'' +
                    ", dst='" + dst + '\'' +
                    '}';
        }
    }

    private List<Trans_result> trans_result;//翻译结果,包含了src和dst字段


    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return this.from;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getTo() {
        return this.to;
    }

    public void setTrans_result(List<Trans_result> trans_result) {
        this.trans_result = trans_result;
    }

    public List<Trans_result> getTrans_result() {
        return this.trans_result;
    }

    @Override
    public String toString() {
        return "BaiduTranslationResult{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", trans_result=" + trans_result +
                '}';
    }
}

