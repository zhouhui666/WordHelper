package ncist.edu.cn.entity;


import cn.bmob.v3.BmobObject;

public class MyWord extends BmobObject{

    private String mobile;
    private String userid;
    private String id;
    private int status;//0为已掌握数字越大，越不熟悉，默认为3，认识减1，不认识加3

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MyWord{" +
                "mobile='" + mobile + '\'' +
                ", userid='" + userid + '\'' +
                ", id='" + id + '\'' +
                ", status=" + status +
                '}';
    }
}
