package ncist.edu.cn.entity;

import cn.bmob.v3.BmobObject;

public class NewWords extends BmobObject{

    private String userId;

    private String mobile;

    private String spelling;

    private String meanning;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSpelling() {
        return spelling;
    }

    public void setSpelling(String spelling) {
        this.spelling = spelling;
    }

    public String getMeanning() {
        return meanning;
    }

    public void setMeanning(String meanning) {
        this.meanning = meanning;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NewWords)) return false;

        NewWords newWords = (NewWords) o;

        if (getUserId() != null ? !getUserId().equals(newWords.getUserId()) : newWords.getUserId
                () != null)
            return false;
        if (getMobile() != null ? !getMobile().equals(newWords.getMobile()) : newWords.getMobile
                () != null)
            return false;
        if (!getSpelling().equals(newWords.getSpelling())) return false;
        return getMeanning().equals(newWords.getMeanning());

    }

    @Override
    public int hashCode() {
        int result = getUserId() != null ? getUserId().hashCode() : 0;
        result = 31 * result + (getMobile() != null ? getMobile().hashCode() : 0);
        result = 31 * result + getSpelling().hashCode();
        result = 31 * result + getMeanning().hashCode();
        return result;
    }
}
