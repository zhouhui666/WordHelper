package ncist.edu.cn.entity;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ncist.edu.cn.utils.Constant;

/**
 * 单词，即从数据库读出的单词
 */
@DatabaseTable(tableName = Constant.DB_TABLE_DBWORD)
public class DBWord {

    @DatabaseField(generatedId = true)
    private int objectId;
    /**
     * 标识号,数据库为String类型
     */
    @DatabaseField(columnName = "ID")
    private String id;
    /**
     * 拼写
     */
    @DatabaseField(columnName = "SPELLING")
    private String spelling;
    /**
     * 含义
     */
    @DatabaseField(columnName = "MEANNING")
    private String meaning;
    /**
     * 发音
     */
    @DatabaseField(columnName = "PHONETIC_ALPHABET")
    private String phoneticAlphabet;

    @DatabaseField(columnName = "STATUS",canBeNull = true)
    private Integer status;

    public DBWord() {
        super();
    }

    public DBWord(String id, String spelling, String meaning,
                  String phoneticAlphabet,Integer status) {
        super();
        this.id = id;
        this.spelling = spelling;
        this.meaning = meaning;
        this.phoneticAlphabet = phoneticAlphabet;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSpelling() {
        return spelling;
    }

    public void setSpelling(String spelling) {
        this.spelling = spelling;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public String getPhoneticAlphabet() {
        return phoneticAlphabet;
    }

    public void setPhoneticAlphabet(String phoneticAlphabet) {
        this.phoneticAlphabet = phoneticAlphabet;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DBWord{" +
                "id='" + id + '\'' +
                ", spelling='" + spelling + '\'' +
                ", meaning='" + meaning + '\'' +
                ", phoneticAlphabet='" + phoneticAlphabet + '\'' +
                ", status=" + status +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DBWord)) return false;

        DBWord dbWord = (DBWord) o;

        if (!getId().equals(dbWord.getId())) return false;
        if (!getSpelling().equals(dbWord.getSpelling())) return false;
        if (!getMeaning().equals(dbWord.getMeaning())) return false;
        if (!getPhoneticAlphabet().equals(dbWord.getPhoneticAlphabet())) return false;
        return getStatus().equals(dbWord.getStatus());

    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + getSpelling().hashCode();
        result = 31 * result + getMeaning().hashCode();
        result = 31 * result + getPhoneticAlphabet().hashCode();
        return result;
    }
}
