package ncist.edu.cn.entity;


import java.io.Serializable;

import cn.bmob.v3.BmobObject;

public class MyLearningRecord  extends BmobObject implements Serializable{
    private String userId;
    private String userName;
    private String mobile;
    private int days;
    private int learnedWords;
    private int masteredWords;
    private int todayWords;
    private int todayNewWords;
    private int todayLearnedWords;
    private int lastStartWordId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getLearnedWords() {
        return learnedWords;
    }

    public void setLearnedWords(int learnedWords) {
        this.learnedWords = learnedWords;
    }

    public int getMasteredWords() {
        return masteredWords;
    }

    public void setMasteredWords(int masteredWords) {
        this.masteredWords = masteredWords;
    }

    public int getTodayWords() {
        return todayWords;
    }

    public void setTodayWords(int todayWords) {
        this.todayWords = todayWords;
    }

    public int getTodayNewWords() {
        return todayNewWords;
    }

    public void setTodayNewWords(int todayNewWords) {
        this.todayNewWords = todayNewWords;
    }

    public int getTodayLearnedWords() {
        return todayLearnedWords;
    }

    public void setTodayLearnedWords(int todayLearnedWords) {
        this.todayLearnedWords = todayLearnedWords;
    }

    public int getLastStartWordId() {
        return lastStartWordId;
    }

    public void setLastStartWordId(int lastStartWordId) {
        this.lastStartWordId = lastStartWordId;
    }

    @Override
    public String toString() {
        return "MyLearningRecord{" +
                "userId='" + userId + '\'' +
                ", userName='" + userName + '\'' +
                ", mobile='" + mobile + '\'' +
                ", days=" + days +
                ", learnedWords=" + learnedWords +
                ", masteredWords=" + masteredWords +
                ", todayWords=" + todayWords +
                ", todayNewWords=" + todayNewWords +
                ", todayLearnedWords=" + todayLearnedWords +
                ", lastStartWordId=" + lastStartWordId +
                '}';
    }
}
