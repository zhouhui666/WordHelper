package ncist.edu.cn.entity;


import java.util.List;

public class YouDaoResult {

    private List<Object> translation;

    private Basic basic;

    private String query;

    private int errorCode;

    private List<Web> web;

    public void setTranslation(List<Object> translation) {
        this.translation = translation;
    }

    public List<Object> getTranslation() {
        return this.translation;
    }

    public void setBasic(Basic basic) {
        this.basic = basic;
    }

    public Basic getBasic() {
        return this.basic;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getQuery() {
        return this.query;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return this.errorCode;
    }

    public void setWeb(List<Web> web) {
        this.web = web;
    }

    public List<Web> getWeb() {
        return this.web;
    }

    public class Basic {
        private String us_phonetic;

        private String phonetic;

        private String uk_phonetic;

        private List<Object> explains;

        public void setUs_phonetic(String us_phonetic) {
            this.us_phonetic = us_phonetic;
        }

        public String getUs_phonetic() {
            return this.us_phonetic;
        }

        public void setPhonetic(String phonetic) {
            this.phonetic = phonetic;
        }

        public String getPhonetic() {
            return this.phonetic;
        }

        public void setUk_phonetic(String uk_phonetic) {
            this.uk_phonetic = uk_phonetic;
        }

        public String getUk_phonetic() {
            return this.uk_phonetic;
        }

        public void setExplains(List<Object> explains) {
            this.explains = explains;
        }

        public List<Object> getExplains() {
            return this.explains;
        }

        @Override
        public String toString() {
            return "Basic{" +
                    "us_phonetic='" + us_phonetic + '\'' +
                    ", phonetic='" + phonetic + '\'' +
                    ", uk_phonetic='" + uk_phonetic + '\'' +
                    ", explains=" + explains +
                    '}';
        }
    }

    public class Web {
        private List<Object> value;

        private String key;

        public void setValue(List<Object> value) {
            this.value = value;
        }

        public List<Object> getValue() {
            return this.value;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getKey() {
            return this.key;
        }

        @Override
        public String toString() {
            return "Web{" +
                    "value=" + value +
                    ", key='" + key + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "YouDaoResult{" +
                "translation=" + translation +
                ", basic=" + basic +
                ", query='" + query + '\'' +
                ", errorCode=" + errorCode +
                ", web=" + web +
                '}';
    }
}
