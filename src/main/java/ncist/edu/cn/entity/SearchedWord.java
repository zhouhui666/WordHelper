package ncist.edu.cn.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import ncist.edu.cn.utils.Constant;

@DatabaseTable(tableName = Constant.DB_TABLE_SEARCHED_WORD)
public class SearchedWord {
    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String query;
    @DatabaseField
    private String translation;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    @Override
    public String toString() {
        return "SearchedWord{" +
                "id=" + id +
                ", query='" + query + '\'' +
                ", translation='" + translation + '\'' +
                '}';
    }
}
