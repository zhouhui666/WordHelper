package ncist.edu.cn.dal;


import android.content.Context;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.utils.DBUtils;

public class DBWordDao {
    private Dao<DBWord,Integer> dao;

    public DBWordDao(Context context){
        try {
            dao = DBUtils.newInstance(context).getDao(DBWord.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public int add(DBWord dbWord) {
        try {
            return dao.create(dbWord);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int deleteAll(List<DBWord> dbWords){
        try {
            return dao.delete(dbWords);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int delete(DBWord dbWord) {
        try {
            return dao.delete(dbWord);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int delete(int id) {
        try {
            return dao.deleteById(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int update(DBWord dbWord) {
        try {
            return dao.update(dbWord);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public List<DBWord> queryAll(){
        List<DBWord> words = new ArrayList<>();
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return words;
    }

    public DBWord queryByDBWord(DBWord dbWord) {
        try {
            return dao.queryForSameId(dbWord);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public DBWord queryById(int id){
        try {
            return dao.queryForId(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<DBWord> queryForMap(Map<String,Object> map)  {
        try {
            return dao.queryForFieldValues(map);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
