package ncist.edu.cn.dal;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;

public class DBDao {
    private DBManager manager;
    private SQLiteDatabase db;

    public DBDao(Context context) {
        super();
        manager = new DBManager(context);
    }

    /**
     * 中英文模糊查询
     */
    public List<DBWord> queryByMeanning(String query) {
        int result = CommonUtils.judgeContent(query);
        List<DBWord> words = new ArrayList<DBWord>();
        DBWord word;
        Cursor c = null;
        db = manager.openDatabase();
        if (result == 2) {
            c = db.query(Constant.DB_TABLE_WORD, null, "MEANNING like ? LIMIT 0,10", new String[]{"%"
                    + query
                    + "%"}, null, null, null);
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                word = new DBWord();
                word.setId(c.getString(c.getColumnIndex("ID")));
                word.setSpelling(c.getString(c.getColumnIndex("SPELLING")));
                word.setMeaning(c.getString(c.getColumnIndex("MEANNING")));
                word.setPhoneticAlphabet(c.getString(c
                        .getColumnIndex("PHONETIC_ALPHABET")));
                words.add(word);
            }
        } else if (result == 1) {
            c = db.query(Constant.DB_TABLE_WORD, null, "SPELLING like ? LIMIT 0,10", new String[]{query
                    + "%"}, null, null, null);
            for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
                word = new DBWord();
                word.setId(c.getString(c.getColumnIndex("ID")));
                word.setSpelling(c.getString(c.getColumnIndex("SPELLING")));
                word.setMeaning(c.getString(c.getColumnIndex("MEANNING")));
                word.setPhoneticAlphabet(c.getString(c
                        .getColumnIndex("PHONETIC_ALPHABET")));
                words.add(word);
            }
        }
        c.close();
        manager.closeDatabase();
        return words;
    }

    public List<DBWord> getDBWordsById(String ids){
        List<DBWord> words = new ArrayList<>();
        DBWord word ;
        Cursor c ;
        db = manager.openDatabase();
        c = db.rawQuery("SELECT * FROM word WHERE ID IN("+ids+")",null);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            word = new DBWord();
            word.setId(c.getString(c.getColumnIndex("ID")));
            word.setSpelling(c.getString(c.getColumnIndex("SPELLING")));
            word.setMeaning(c.getString(c.getColumnIndex("MEANNING")));
            word.setPhoneticAlphabet(c.getString(c
                    .getColumnIndex("PHONETIC_ALPHABET")));
            words.add(word);
        }
        return words;
    }

    /**
     * 根据最后开始的id和单词数量获取下一轮单词的id
     * @param number 下一轮单词数量
     * @param lastStartId 上一轮最后单词的id
     * @return 返回下一轮单词的id
     */
    public String getIdsByWordsAndStartId(int number, int lastStartId) {
        int total = Constant.DB_WORD_NUMBER;
        int count = 100;
        StringBuilder builder = new StringBuilder();
        int currentId = lastStartId + count;
        if ( lastStartId == 1){
            currentId = 1;
        }
        if (number > 1) {
            for (int i = 0; i < number; i++) {
                if (currentId <= total) {
                    builder.append(currentId + ",");
                } else {
                    currentId = currentId - currentId / 100 * 100 + 1;
                    builder.append(currentId + ",");
                }
                currentId += count;
            }
            return builder.substring(0, builder.toString().length() - 1);
        }
        return "-1";
    }
}
