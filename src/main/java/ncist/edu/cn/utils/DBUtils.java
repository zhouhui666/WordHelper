package ncist.edu.cn.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.entity.SearchedWord;

public class DBUtils extends OrmLiteSqliteOpenHelper {

    private static DBUtils dbUtils;

    public DBUtils(Context context) {
        super(context, Constant.MY_DB, null, 1);
    }

    public synchronized static DBUtils newInstance(Context context) {
        if (dbUtils == null) {
            dbUtils = new DBUtils(context);
        }
        return dbUtils;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0, ConnectionSource arg1) {
        try {
            TableUtils.createTableIfNotExists(arg1, SearchedWord.class);
            TableUtils.createTableIfNotExists(arg1, DBWord.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
                          int arg3) {
        try {
            TableUtils.dropTable(arg1, SearchedWord.class, true);
            TableUtils.dropTable(arg1,DBWord.class,true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
