package ncist.edu.cn.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {

    public static void showToast(final Activity activity, final String msg) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void showAlertDialog(Context context, String title, String message, DialogInterface.OnClickListener positiveListener,
                                       DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title).setMessage(message).setPositiveButton("确定", positiveListener).setNegativeButton("取消", negativeListener).show();
    }

    /**
     * 格式化json字符串
     */
    public static String formatJson(String json) {
        json = json.replaceAll("-", "_");
        return json;
    }

    public static int judgeContent(String content) {
        int result = -1;
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(content);
        if (m.matches()) {
            result = 0;
        }
        p = Pattern.compile("[a-zA-Z]*");
        m = p.matcher(content);
        if (m.matches()) {
            result = 1;
        }
        p = Pattern.compile("[\u4e00-\u9fa5]*");
        m = p.matcher(content);
        if (m.matches()) {
            result = 2;
        }
        return result;
    }

    public static void hideOrShowInputKeyboard(Context context){
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }


    public static void hideInput(Context context,View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}
