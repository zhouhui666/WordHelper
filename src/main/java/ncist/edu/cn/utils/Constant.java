package ncist.edu.cn.utils;


public class Constant {
    public static final String BMOB_APP_ID = "76f3f534fd6f3dcbe2b46e1ce9b19928";
    public static int SCREEN_HEIGHT  ;
    public static final String MOB_APP_KEY = "11245d50a4c60";
    public static final String MOB_SECRET = "13bd8737300c9b0a9c9708b1c7254a63";
    public static final String IS_FIRST_OPEN = "IS_FIRST_OPEN";
    public static final String BAIDU_TRANSLATION_APP_ID = "20160404000017547";
    public static final String BAIDU_TRANSLATION_SCREAT = "veC4TtDjcsBPQy_Qne_e";
    public static final String DEFAULT_ENCODE = "UTF-8";
    public static final String BAIDU_TRANSLATION_URL = "http://api.fanyi.baidu.com/api/trans/vip/translate?";
    public static final String LANGUAGE_ZH = "zh";
    public static final String LANGUAGE_EN = "en";
    public static final String LANGUAGE_JP = "jp";
    public static final String LANGUAGE_KOR = "kor";
    public static final String LANGUAGE_FRA = "fra";
    public static final String LANGUAGE_AUTO = "auto";
    public static final String YOUDAO_API_KEY = "506696824";
    public static final String YOUDAO_KEY_FROM = "ZuoZuoshenghen";
    public static final String MY_DB = "database.db";
    public static final String DB_TABLE_SEARCHED_WORD = "searches";
    public static final int DB_WORD_NUMBER = 6276;
    public static final String DB_TABLE_WORD = "word";
    public static final String DB_TABLE_DBWORD = "words";
    public static String MOBILE = "";
    //头像路径
    public static String MY_AVATAR_PATH = "avatar/";
    //修改用户头像时，可以拍照或从图库中选择，发送一个混合Intent的RequestCode
    public static final int REQUESTCODE_UPLOADAVATAR_CAMERA_OR_MEDIASTORE = 0x110;
    //系统裁剪头像
    public static final int REQUESTCODE_UPLOADAVATAR_CROP = 0x111;

}
