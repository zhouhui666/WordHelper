package ncist.edu.cn.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.DBWord;

public class DBWordAdapter extends BaseAdapter {

    private List<DBWord> words;
    private LayoutInflater inflater;

    public DBWordAdapter(Context context, List<DBWord> words) {
        if (words == null) {
            words = new ArrayList<>();
        }
        this.words = words;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return words.size();
    }

    @Override
    public DBWord getItem(int position) {
        return words.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_dictionary_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvQuery.setText(words.get(position).getSpelling());
        viewHolder.tvTranslation.setText(words.get(position).getMeaning());
        return convertView;
    }

    static class ViewHolder {

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @Bind(R.id.item_dictionary_tv_query)
        TextView tvQuery;
        @Bind(R.id.item_dictionary_tv_translation)
        TextView tvTranslation;
    }
}
