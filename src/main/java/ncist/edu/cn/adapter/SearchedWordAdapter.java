package ncist.edu.cn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.SearchedWord;

public class SearchedWordAdapter extends BaseAdapter {

    private List<SearchedWord> searchedWords;
    private LayoutInflater inflater;

    public SearchedWordAdapter(Context context, List<SearchedWord> searchedWords) {
        if (searchedWords == null) {
            searchedWords = new ArrayList<>();
        }
        this.searchedWords = searchedWords;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return searchedWords.size();
    }

    @Override
    public SearchedWord getItem(int position) {
        return searchedWords.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder ;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.layout_dictionary_item,null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvQuery.setText(searchedWords.get(position).getQuery());
        if(null != searchedWords.get(position).getTranslation()) {
            viewHolder.tvTranslation.setText(searchedWords.get(position).getTranslation());
        }
        return convertView;
    }

    static class ViewHolder {

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @Bind(R.id.item_dictionary_tv_query)
        TextView tvQuery;
        @Bind(R.id.item_dictionary_tv_translation)
        TextView tvTranslation;
    }
}
