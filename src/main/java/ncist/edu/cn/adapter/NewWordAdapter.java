package ncist.edu.cn.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.NewWords;

public class NewWordAdapter extends BaseAdapter {

    private List<NewWords> newWordses;
    private LayoutInflater inflater;

    public NewWordAdapter(Context context, List<NewWords> newWordses) {
        if (newWordses == null) {
            newWordses = new ArrayList<>();
        }
        this.newWordses = newWordses;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return newWordses.size();
    }

    @Override
    public NewWords getItem(int position) {
        return newWordses.get(position);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_dictionary_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvQuery.setText(newWordses.get(position).getSpelling());
        viewHolder.tvTranslation.setText(newWordses.get(position).getMeanning());
        return convertView;
    }

    static class ViewHolder {

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

        @Bind(R.id.item_dictionary_tv_query)
        TextView tvQuery;
        @Bind(R.id.item_dictionary_tv_translation)
        TextView tvTranslation;
    }
}
