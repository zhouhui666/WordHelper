package ncist.edu.cn.view;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import ncist.edu.cn.R;

public class AlphaImageView extends ImageView{

    private Drawable mdrawalbe;

    public AlphaImageView(Context context){
        this(context,null);
    }

    public AlphaImageView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public AlphaImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    public void init(Context context,AttributeSet attrs){
        TypedArray ta = context.obtainStyledAttributes(attrs,
                R.styleable.AlphaImageView);
        mdrawalbe = ta.getDrawable(R.styleable.AlphaImageView_image_drawable);
//        mdrawalbe.getDrawable
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }
}
