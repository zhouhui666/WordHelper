package ncist.edu.cn.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.image.ImageInfo;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.UpdateListener;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.moments.WechatMoments;
import ncist.edu.cn.R;
import ncist.edu.cn.activity.RememberActivity;
import ncist.edu.cn.dal.BmobDao;
import ncist.edu.cn.entity.CiBaResult;
import ncist.edu.cn.entity.MyLearningRecord;
import ncist.edu.cn.utils.DownloadUtils;
import ncist.edu.cn.utils.FilePathUtils;
import ncist.edu.cn.utils.GsonUtils;
import ncist.edu.cn.utils.HttpUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;
import ncist.edu.cn.utils.TimeUtils;

public class ReciteFragment extends Fragment implements PlatformActionListener {

    @Bind(R.id.recite_iv_introduce)
    SimpleDraweeView simpleDraweeView;
    @Bind(R.id.recite_tv_introduce)
    TextView tvIntroduce;
    @Bind(R.id.recite_iv_qq)
    ImageView reciteIvQq;
    @Bind(R.id.recite_iv_weixin)
    ImageView reciteIvWeixin;
    @Bind(R.id.recite_iv_weibo)
    ImageView reciteIvWeibo;
    @Bind(R.id.recite_btn_start)
    Button reciteBtnStart;
    @Bind(R.id.recite_tv_days)
    TextView reciteTvDays;
    @Bind(R.id.recite_tv_learned_words)
    TextView reciteTvLearnedWords;
    @Bind(R.id.recite_tv_master_words)
    TextView reciteTvMasterWords;
    @Bind(R.id.recite_tv_today_words)
    TextView reciteTvTodayWords;
    @Bind(R.id.recite_tv_today_new_words)
    TextView reciteTvTodayNewWords;
    @Bind(R.id.recite_tv_today_learned_word)
    TextView reciteTvTodayLearnedWord;
    @Bind(R.id.recite_ll_top)
    LinearLayout reciteLlTop;

    private Handler handler;
    private final int PARSE_SUCCESS = 1;
    private CiBaResult ciBaResult;
    private final int REMEMBER_ACTIVITY = 1;
    private int masterWordsCount = 0;
    private int todayNewWordsCount = 0;
    private int todayLearnedWordsCount = 0;
    private int todayWordsCount = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
            setRetainInstance(false);
            ShareSDK.initSDK(getActivity());
            Fresco.initialize(getActivity());
            View view = inflater.inflate(R.layout.fargment_recite_layout, null);
            ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
        handler = new Handler(new InnerCallback());
        parseJson();
    }

    public void parseJson() {
        String url = "http://open.iciba.com/dsapi";
        HttpUtils.doGetAsyn(url, new HttpUtils.CallBack() {
            @Override
            public void onRequestComplete(String result) {
                if (result != null) {
                    ciBaResult = GsonUtils.getPerson(result, CiBaResult.class);
                    Message.obtain(handler, PARSE_SUCCESS).sendToTarget();
                }
            }
        });
    }

    @OnClick({R.id.recite_iv_qq, R.id.recite_iv_weixin, R.id.recite_iv_weibo, R.id.recite_btn_start})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.recite_iv_qq:
                QZone.ShareParams sp = new QZone.ShareParams();
                sp.setTitle("每日一句");
                sp.setTitleUrl("http://news.iciba.com/dailysentence/detail-2014.html"); // 标题的超链接
                if(ciBaResult != null) {
                    sp.setText(ciBaResult.getContent()+"\n"+ciBaResult.getNote());
                    sp.setImageUrl(ciBaResult.getPicture());
                }
                sp.setSite("单词小助手");
                sp.setSiteUrl("http://news.iciba.com/dailysentence");
                Platform qzone = ShareSDK.getPlatform(QZone.NAME);
                qzone.setPlatformActionListener(this); // 设置分享事件回调
                // 执行图文分享
                qzone.share(sp);
                break;
            case R.id.recite_iv_weixin:
                WechatMoments.ShareParams sp2 = new WechatMoments.ShareParams();
                sp2.setShareType(Platform.SHARE_WEBPAGE);
                sp2.setImagePath("/mnt/sdcard/测试分享的图片.jpg");
                if(ciBaResult != null) {
                    sp2.setImageUrl(ciBaResult.getPicture());
                    sp2.setText(ciBaResult.getContent() + "\n" + ciBaResult.getNote());
                }
                sp2.setImageData(null);
                Platform weixin = ShareSDK.getPlatform(WechatMoments.NAME);
                weixin.setPlatformActionListener(this);
                weixin.share(sp2);
                break;
            case R.id.recite_iv_weibo:
                SinaWeibo.ShareParams sp3 = new SinaWeibo.ShareParams();
                String text = "#单词小助手每日一句# " + ciBaResult.getContent() + " " + ciBaResult.getNote();
                sp3.setText(text);
                sp3.setImagePath("/mnt/sdcard/测试分享的图片.jpg");
                if(ciBaResult != null) {
                    sp3.setUrl(ciBaResult.getPicture());
                }
                Platform weibo = ShareSDK.getPlatform(SinaWeibo.NAME);
                weibo.setPlatformActionListener(this); // 设置分享事件回调
                // 执行图文分享
                weibo.share(sp3);
                break;
            case R.id.recite_btn_start:
                Intent intent = new Intent(getActivity(), RememberActivity.class);
                startActivityForResult(intent, REMEMBER_ACTIVITY);
                break;
        }
    }

    private class InnerCallback implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case PARSE_SUCCESS:
                    String url = ciBaResult.getPicture();
                    Uri uri = Uri.parse(url);
                    if(uri != null) {
                        simpleDraweeView.setImageURI(uri);
                    }
                    tvIntroduce.setText(ciBaResult.getContent() + "\n" + ciBaResult.getNote());
                    break;
            }
            return false;
        }
    }

    public void download(Context context) {
        String fileName = "img" + ciBaResult.getPicture().substring(ciBaResult.getPicture()
                .lastIndexOf("/"));
        String path = FilePathUtils.getFilePath(context, "images");
        DownloadUtils.download(ciBaResult.getPicture(), path, fileName, handler);
        fileName = "share" + ciBaResult.getFenxiang_img().substring(ciBaResult.getFenxiang_img()
                .lastIndexOf("/"));
        DownloadUtils.download(ciBaResult.getFenxiang_img(), path, fileName, handler);
    }

    public void show(Context context) {
        String fileName = "img" + ciBaResult.getPicture().substring(ciBaResult.getPicture()
                .lastIndexOf("/"));
        File file = new File(new File(FilePathUtils.getFilePath(context, "images")), fileName);
        if (!file.exists()) {
            download(context);
        } else {
            simpleDraweeView.setImageURI(Uri.parse("file://" + file.getAbsolutePath()));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {
                case REMEMBER_ACTIVITY:
                    // 接收已完成的单词
                    todayLearnedWordsCount = data.getIntExtra("todayLearnedWordsCount", 0);
                    MyLearningRecord result = (MyLearningRecord) data.getSerializableExtra("record");
                    if (result != null) {
                        todayLearnedWordsCount = result.getTodayLearnedWords();
                    }
                    reciteTvTodayLearnedWord.setText(todayLearnedWordsCount+"");
                    break;
            }
        }
    }

    @Override
    public void onResume() {
        if(null != getContext()){
            initData();
        }
        super.onResume();
    }

    public void initData(){
        String userId = SharedPreferencesUtils.getString(getContext(),"userId");
        Map<String,Object> map = new HashMap<>();
        map.put("userId",userId);
        BmobDao.queryByMap(getContext(), map, MyLearningRecord.class, new FindListener<MyLearningRecord>() {
            @Override
            public void onSuccess(List<MyLearningRecord> list) {
                todayWordsCount = SharedPreferencesUtils.getInteger(getContext(),"todayWordsCount",20);
                if(list != null && list.size() > 0){
                    MyLearningRecord record = list.get(0);
                    reciteTvDays.setText(record.getDays()+"");
                    reciteTvMasterWords.setText(record.getMasteredWords()+"");
                    reciteTvLearnedWords.setText(record.getLearnedWords()+"");
                    reciteTvTodayNewWords.setText(record.getTodayNewWords()+"");
                    reciteTvTodayWords.setText(todayWordsCount+"");
                    reciteTvTodayLearnedWord.setText(record.getTodayLearnedWords()+"");
                    if(record.getTodayLearnedWords() != 0){
                        reciteBtnStart.setVisibility(View.GONE);
                    }
                    String lastLearnedTime = record.getUpdatedAt();
                    lastLearnedTime = lastLearnedTime.substring(0,10);
                    if(!lastLearnedTime.equals(TimeUtils.getSimpleDateStringTimeNow())){
                        record.setTodayLearnedWords(0);
                        record.update(getActivity(), record.getObjectId(), new UpdateListener() {
                            @Override
                            public void onSuccess() {
                                Log.d("debug","次日更新成功!");
                            }

                            @Override
                            public void onFailure(int i, String s) {
                                Log.e("error",i+s);
                            }
                        });
                    }

                }else{
                    reciteTvDays.setText(0+"");
                    reciteTvTodayLearnedWord.setText(0+"");
                    reciteTvMasterWords.setText(0+"");
                    reciteTvTodayWords.setText(todayWordsCount+"");
                    reciteTvTodayNewWords.setText(todayWordsCount+"");
                    reciteTvTodayLearnedWord.setText(todayLearnedWordsCount+"");
                }
            }

            @Override
            public void onError(int i, String s) {
                Log.e("error",i+s);
            }
        });

    }

    private ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
        @Override
        public void onFinalImageSet(String id, ImageInfo imageInfo, Animatable animatable) {

        }
    };

    @Override
    public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {

    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {

    }

    @Override
    public void onCancel(Platform platform, int i) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onDestroy() {
        ButterKnife.unbind(this);
        super.onDestroy();
    }
}
