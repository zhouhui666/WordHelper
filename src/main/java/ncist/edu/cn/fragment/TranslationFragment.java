package ncist.edu.cn.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import ncist.edu.cn.R;
import ncist.edu.cn.entity.BaiduTranslationResult;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.GsonUtils;
import ncist.edu.cn.utils.HttpUtils;
import ncist.edu.cn.utils.MD5Utils;
import ncist.edu.cn.utils.NetworkUtils;

public class TranslationFragment extends Fragment {


    @Bind(R.id.translation_et_from)
    EditText translationEtFrom;
    @Bind(R.id.translation_spinner)
    Spinner translationSpinner;
    @Bind(R.id.translation_btn_translation)
    Button translationBtnTranslation;
    @Bind(R.id.translation_tv_to)
    TextView translationTvTo;
    @Bind(R.id.translation_iv_share)
    ImageView translationIvShare;
    @Bind(R.id.translation_iv_copy)
    ImageView translationIvCopy;
    @Bind(R.id.translation_iv_delete)
    ImageView translationIvDelete;

    private Handler handler;
    private static final int MSG_TRANSLATION_SUCCESS = 1;
    private String currentTo;
    private String currentFrom;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        setRetainInstance(false);
        View view = inflater.inflate(R.layout.fargment_translation_layout, container,false);
        ButterKnife.bind(this, view);
        ShareSDK.initSDK(getActivity());
        init();
        handler = new Handler(new InnerCallBack());
        return view;
    }

    public void init() {
        translationEtFrom.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() >0){
                    translationIvDelete.setVisibility(View.VISIBLE);
                    translationBtnTranslation.setEnabled(true);
                }else{
                    translationIvDelete.setVisibility(View.INVISIBLE);
                    translationBtnTranslation.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        translationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentTo = translationSpinner.getItemAtPosition(position).toString();
                selectTo(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * 判断目标语言
     */
    public boolean translateToEnglish(String from) {
        Pattern pattern = Pattern.compile("[\\u4e00-\\u9fa5]");
        Matcher matcher = pattern.matcher(from);
        return matcher.find();//判断是否包含中文
    }

    /**
     * 选择源语和目标语言
     */
    public void selectTo(int position) {
        switch (position) {
            case 0:
                currentFrom = Constant.LANGUAGE_AUTO;
                break;
            case 1:
                currentFrom = Constant.LANGUAGE_EN;
                currentTo = Constant.LANGUAGE_ZH;
                break;
            case 2:
                currentFrom = Constant.LANGUAGE_ZH;
                currentTo = Constant.LANGUAGE_EN;
                break;
            case 3:
                currentFrom = Constant.LANGUAGE_JP;
                currentTo = Constant.LANGUAGE_ZH;
                break;
            case 4:
                currentFrom = Constant.LANGUAGE_ZH;
                currentTo = Constant.LANGUAGE_JP;
                break;
            case 5:
                currentFrom = Constant.LANGUAGE_KOR;
                currentTo = Constant.LANGUAGE_ZH;
                break;
            case 6:
                currentFrom = Constant.LANGUAGE_ZH;
                currentTo = Constant.LANGUAGE_KOR;
                break;
            case 7:
                currentFrom = Constant.LANGUAGE_FRA;
                currentTo = Constant.LANGUAGE_ZH;
            case 8:
                currentFrom = Constant.LANGUAGE_ZH;
                currentTo = Constant.LANGUAGE_FRA;
                break;
        }
    }

    /**
     * 翻译
     */
    public void translation(String query) {
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            String q;
            try {
                if (currentFrom.equals(Constant.LANGUAGE_AUTO)) {
                    if (translateToEnglish(query)) {
                        currentTo = Constant.LANGUAGE_EN;
                    } else {
                        currentTo = Constant.LANGUAGE_ZH;
                    }
                }
                String salt = String.valueOf(System.currentTimeMillis()).substring(0, 10);
                //计算签名sign（对字符串1做md5加密，注意计算md5之前，串1必须为UTF-8编码）
                String sign = MD5Utils.MD5(Constant.BAIDU_TRANSLATION_APP_ID + query + salt +
                        Constant.BAIDU_TRANSLATION_SCREAT).toLowerCase();
                q = URLEncoder.encode(query, Constant.DEFAULT_ENCODE);
                //http://api.fanyi.baidu.com/api/trans/vip/translate?q=apple&from=en&to=zh&appid
                // =2015063000000001&salt=1435660288&sign
                // =f89f9594663708c1605f3d736d01d2d4
                String url = Constant.BAIDU_TRANSLATION_URL + "q=" + q + "&from=" + currentFrom +
                        "&to=" + currentTo + "&appid=" + Constant.BAIDU_TRANSLATION_APP_ID +
                        "&salt=" + salt + "&sign=" + sign;
                HttpUtils.doGetAsyn(url, new HttpUtils.CallBack() {
                    @Override
                    public void onRequestComplete(String result) {
                        BaiduTranslationResult translationResult = GsonUtils.getPerson(result,
                                BaiduTranslationResult.class);
                        Message.obtain(handler, MSG_TRANSLATION_SUCCESS, translationResult)
                                .sendToTarget();
                    }
                });
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }else {
            CommonUtils.showToast(getActivity(),"网络连接不可用,请检查网络设置!");
        }
    }

    private class InnerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_TRANSLATION_SUCCESS:
                    BaiduTranslationResult result = (BaiduTranslationResult) msg.obj;
                    if (result != null) {
                        Log.d("info", "result:" + result);
                        translationTvTo.setVisibility(View.VISIBLE);
                        translationTvTo.setText("译文:\n");
                        if (result.getTrans_result() != null) {
                            translationTvTo.append(result.getTrans_result().get(0).getDst());
                        }
                    } else {
                        CommonUtils.showToast(getActivity(), "解析失败!");
                    }
                    break;
            }
            return false;
        }

    }

    @OnClick({R.id.translation_btn_translation, R.id.translation_iv_share, R.id
            .translation_iv_copy,R.id.translation_iv_delete})
    public void onClick(View view) {
        String temp = translationTvTo.getText().toString().trim();
        String to = temp.substring(temp.indexOf(":") + 1, temp.length());
        String text = translationEtFrom.getText().toString().trim() + " 的译文是:" + to + ",来自@单词小助手";
        switch (view.getId()) {
            case R.id.translation_btn_translation:
                String query = translationEtFrom.getText().toString().trim();
                translation(query);
                translationIvCopy.setVisibility(View.VISIBLE);
                translationIvShare.setVisibility(View.VISIBLE);
                CommonUtils.hideInput(getContext(),translationEtFrom);
                break;
            case R.id.translation_iv_share:
                share(text, "");
                break;
            case R.id.translation_iv_copy:
                copy(getActivity(), to);
                break;
            case R.id.translation_iv_delete:
                translationEtFrom.getText().clear();
                break;
        }
    }

    public void copy(Context context, String text) {
        ClipboardManager clipboardManager = (ClipboardManager) context.getSystemService(Context
                .CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText(null, text.trim()));
        CommonUtils.showToast(getActivity(),"已经复制到剪贴板");
    }

    public void share(String text, String comment) {
        OnekeyShare oks = new OnekeyShare();
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
        //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(getString(R.string.app_name));
        // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
        oks.setTitleUrl("http://sharesdk.cn");
        // text是分享文本，所有平台都需要这个字段
        oks.setText(text);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        oks.setImagePath("");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl("http://sharesdk.cn");
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment(comment);
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(getString(R.string.app_name));
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl("http://sharesdk.cn");
        // 启动分享GUI
        oks.show(getActivity());
    }

    @Override
    public void onDestroyView() {
        ButterKnife.unbind(this);
        ShareSDK.stopSDK(getActivity());
        super.onDestroyView();
    }
}
