package ncist.edu.cn.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ncist.edu.cn.R;
import ncist.edu.cn.activity.FeedbackActivity;
import ncist.edu.cn.activity.LoginActivity;
import ncist.edu.cn.activity.MyAccountActivity;
import ncist.edu.cn.activity.NewWordsActivity;
import ncist.edu.cn.activity.SettingActivity;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.SharedPreferencesUtils;

public class SettingsFragment extends Fragment {

    private static final int LOGIN_SUCCEED = 1;
    @Bind(R.id.settings_home)
    Button settingsHome;
    @Bind(R.id.settings_feedback)
    Button settingsFeedback;
    @Bind(R.id.settings_btn_settings)
    Button settingsBtnSettings;
    @Bind(R.id.settings_btn_logout)
    Button settingsBtnLogout;
    @Bind(R.id.settings_btn_login_or_register)
    Button settingsBtnLoginOrRegister;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        setRetainInstance(false);//Activity被销毁被销毁时，fragment也会被销毁
        View view = inflater.inflate(R.layout.fargment_settings_layout, container, false);
        boolean isLogin = SharedPreferencesUtils.getBoolean(getContext(), "isLogin", false);
        ButterKnife.bind(this, view);
        isLogin(isLogin);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.settings_home, R.id.settings_feedback, R.id.settings_btn_settings, R.id
            .settings_btn_logout, R.id.settings_btn_words
            , R.id.settings_btn_login_or_register, R.id.settings_btn_about})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settings_home:
                Intent intent = new Intent(getActivity(), MyAccountActivity.class);
                startActivity(intent);
                break;
            case R.id.settings_feedback:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                startActivity(intent);
                break;
            case R.id.settings_btn_settings:
                intent = new Intent(getActivity(), SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.settings_btn_logout:
                CommonUtils.showAlertDialog(getContext(), "提示", "是否退出当前账号?", new DialogInterface
                        .OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferencesUtils.putBoolean(getContext(), "isLogin", false);
                        isLogin(false);
                    }
                }, null);
                break;
            case R.id.settings_btn_words:
                intent = new Intent(getActivity(), NewWordsActivity.class);
                startActivity(intent);
                break;
            case R.id.settings_btn_login_or_register:
                intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.settings_btn_about:
                CommonUtils.showAlertDialog(getActivity(), "关于单词小助手", "版本:1.0.0", null, null);
        }
    }

    /**
     * 应该销毁登录的用户所有信息
     *
     * @param isLogin
     */
    public void isLogin(boolean isLogin) {
        if (isLogin) {
            settingsBtnLogout.setVisibility(View.VISIBLE);
            settingsBtnLoginOrRegister.setVisibility(View.GONE);
        } else {
            // 清空手机号,用户信息等
            SharedPreferencesUtils.putString(getActivity(),"userId",null);
            SharedPreferencesUtils.putString(getActivity(), "mobile", null);
            SharedPreferencesUtils.putBoolean(getActivity(), "isLogin", false);
            settingsBtnLogout.setVisibility(View.GONE);
            settingsBtnLoginOrRegister.setVisibility(View.VISIBLE);
        }
    }

}
