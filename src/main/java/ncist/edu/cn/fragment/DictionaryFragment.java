package ncist.edu.cn.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ncist.edu.cn.R;
import ncist.edu.cn.adapter.DBWordAdapter;
import ncist.edu.cn.adapter.SearchedWordAdapter;
import ncist.edu.cn.dal.DBDao;
import ncist.edu.cn.dal.SearchedWordDao;
import ncist.edu.cn.entity.DBWord;
import ncist.edu.cn.entity.SearchedWord;
import ncist.edu.cn.entity.YouDaoResult;
import ncist.edu.cn.utils.CommonUtils;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.DialogUtils;
import ncist.edu.cn.utils.GsonUtils;
import ncist.edu.cn.utils.HttpUtils;
import ncist.edu.cn.utils.NetworkUtils;
import ncist.edu.cn.utils.ValidatorUtil;

public class DictionaryFragment extends Fragment {

    @Bind(R.id.dictionary_et_query)
    EditText dictionaryEtQuery;
    @Bind(R.id.dictionary_iv_search)
    ImageView dictionaryIvSearch;
    @Bind(R.id.dictionary_tv_translation)
    TextView dictionaryTvTranslation;
    @Bind(R.id.dictionary_tv_uk_pronunciation)
    TextView dictionaryTvUkPronunciation;
    @Bind(R.id.dictionary_tv_us_pronunciation)
    TextView dictionaryTvUsPronunciation;
    @Bind(R.id.dictionary_tv_result)
    TextView dictionaryTvResult;
    @Bind(R.id.dictionary_tv_web)
    TextView dictionaryTvWeb;
    @Bind(R.id.dictionary_iv_clear)
    ImageView dictionaryIvClear;
    @Bind(R.id.dictionary_listview)
    ListView listView;
    @Bind(R.id.dictionary_rl_center)
    RelativeLayout rlCenter;
    @Bind(R.id.dictionary_rl_bottom)
    RelativeLayout rlBottom;
    @Bind(R.id.dictionary_scroll)
    ScrollView scrollView;


    private String query = "";
    private Handler handler;
    private static final int RESOLVE_SUCCESS = 1;
    private SearchedWordDao dao;
    private List<SearchedWord> searchWords;
    private SearchedWordAdapter adapter;
    private DBWordAdapter dbWordAdapter;
    private final int MENU_DELETE = 2;
    private final int MENU_DELETE_ALL = 3;
    private DBDao dbDao;
    private List<DBWord> words;
    private final int QUERY_SUCCEED = 4;
    private boolean isSearch = true;
    private Dialog dialog;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable
    Bundle savedInstanceState) {
        setRetainInstance(false);
        View view = inflater.inflate(R.layout.fargment_dictionary_layout, container, false);
        ButterKnife.bind(this, view);
        init();
        handler = new Handler(new InnerCallBack());
        dao = new SearchedWordDao(getActivity());
        dbDao = new DBDao(getActivity());
        words = new ArrayList<>();
        searchWords = getSearchWords();
        adapter = new SearchedWordAdapter(getActivity(), searchWords);
        dbWordAdapter = new DBWordAdapter(getActivity(), words);
        listView.setAdapter(adapter);
        registerForContextMenu(listView);
        return view;
    }

    @OnClick({R.id.dictionary_iv_search, R.id.dictionary_iv_clear})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.dictionary_iv_search:
                query = dictionaryEtQuery.getText().toString().trim();
                if (ValidatorUtil.isNotEmpty(query)) {
                    search(query);
                    CommonUtils.hideInput(getContext(), dictionaryEtQuery);
                    listViewVisible(false);
                }
                break;
            case R.id.dictionary_iv_clear:
                dictionaryEtQuery.getText().clear();
                refresh();
                break;
        }
    }

    /**
     * 初始化控件
     */
    public void init() {
        dictionaryEtQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                query = dictionaryEtQuery.getText().toString();
                if (s.length() == 0) {
                    listView.setAdapter(adapter);
                    dictionaryIvSearch.setEnabled(false);
                    dictionaryIvClear.setVisibility(View.GONE);
                    listViewVisible(true);
                    refresh();
                } else {
                    listView.setAdapter(dbWordAdapter);
                    dictionaryIvClear.setVisibility(View.VISIBLE);
                    dictionaryIvSearch.setEnabled(true);
                    if (isSearch) {
                        listViewVisible(true);
                        refreshByQuery();
                    }
                }
                isSearch = true;
            }
        });
        dictionaryEtQuery.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND || (event != null && event.getKeyCode
                        () == KeyEvent.KEYCODE_ENTER)) {
                    if (query != null) {
                        search(query);
                    }
                    return true;
                }
                return false;
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (query.length() == 0) {
                    query = searchWords.get(position).getQuery();
                } else {
                    if (CommonUtils.judgeContent(query) == 1) {
                        query = words.get(position).getSpelling();
                    } else if (CommonUtils.judgeContent(query) == 2) {
                        query = words.get(position).getMeaning();
                    }
                }
                isSearch = false;
                search(query);
                listViewVisible(false);
                dictionaryEtQuery.setText(query);
                dictionaryEtQuery.setSelection(query.length());
            }
        });
    }

    /**
     * 搜索结果
     */
    public void search(String query) {
        dialog = DialogUtils.createLoadingDialog(getActivity(),"查询中...");
        if (NetworkUtils.isNetworkAvailable(getContext())) {
            try {
                query = URLEncoder.encode(query, Constant.DEFAULT_ENCODE);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            String url = "http://fanyi.youdao.com/openapi.do?keyfrom=" + Constant.YOUDAO_KEY_FROM +
                    "&key=" + Constant.YOUDAO_API_KEY +
                    "&type=data&doctype=json&version=1.1&q=" + query;
            HttpUtils.doGetAsyn(url, new HttpUtils.CallBack() {
                @Override
                public void onRequestComplete(String result) {
                    if (result != null) {
                        dialog.dismiss();
                        dialog = null;
                        Log.d("info", result);
                        result = CommonUtils.formatJson(result);
                        YouDaoResult youDaoResult = GsonUtils.getPerson(result, YouDaoResult.class);
                        Message.obtain(handler, RESOLVE_SUCCESS, youDaoResult).sendToTarget();
                    } else {
                        CommonUtils.showToast(getActivity(), "结果为空!");
                    }
                }
            });
        } else {
            CommonUtils.showToast(getActivity(), "网络连接不可用,请检查网络设置!");
        }
    }

    private class InnerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case RESOLVE_SUCCESS:
                    YouDaoResult youDaoResult = (YouDaoResult) msg.obj;
                    showResult(youDaoResult);
                    save(youDaoResult);
                    CommonUtils.hideInput(getContext(), dictionaryEtQuery);
                    break;
                case QUERY_SUCCEED:
                    List<DBWord> data = (List<DBWord>) msg.obj;
                    words.clear();
                    words.addAll(data);
                    dbWordAdapter.notifyDataSetChanged();
                    break;
            }
            return false;
        }
    }

    /**
     * 展示结果
     */
    public void showResult(YouDaoResult youDaoResult) {
        if (CommonUtils.judgeContent(query) == 1) {
            dictionaryTvUkPronunciation.setVisibility(View.VISIBLE);
            dictionaryTvUsPronunciation.setVisibility(View.VISIBLE);
            if (youDaoResult.getBasic() != null) {
                dictionaryTvUkPronunciation.setText("英:" + youDaoResult.getBasic().getUk_phonetic
                        ());
                dictionaryTvUsPronunciation.setText("美:" + youDaoResult.getBasic().getUs_phonetic
                        ());
            }
        } else {
            dictionaryTvUkPronunciation.setVisibility(View.GONE);
            dictionaryTvUsPronunciation.setVisibility(View.GONE);
        }
        dictionaryTvTranslation.setVisibility(View.VISIBLE);
        if (youDaoResult.getTranslation() != null) {
            dictionaryTvTranslation.setText(youDaoResult.getTranslation().get(0).toString());
        }
        dictionaryTvResult.setVisibility(View.VISIBLE);
        dictionaryTvResult.setText("基本释义:");
        if (youDaoResult.getBasic() != null && youDaoResult.getBasic().getExplains() != null) {
            for (int i = 0; i < youDaoResult.getBasic().getExplains().size(); i++) {
                dictionaryTvResult.append("\n" + youDaoResult.getBasic().getExplains().get(i)
                        .toString());
            }
        }
        dictionaryTvWeb.setVisibility(View.VISIBLE);
        dictionaryTvWeb.setText("网络释义:");
        if (youDaoResult.getWeb() != null) {
            for (int i = 0; i < youDaoResult.getWeb().size(); i++) {
                dictionaryTvWeb.append("\n" + youDaoResult.getWeb().get(i).getKey() + ":");
                for (int j = 0; j < youDaoResult.getWeb().get(i).getValue().size(); j++) {
                    dictionaryTvWeb.append(youDaoResult.getWeb().get(i).getValue().get(j)
                            .toString());

                    if (j < youDaoResult.getWeb().get(i).getValue().size() - 1) {
                        dictionaryTvWeb.append(",");
                    }
                }
            }
        }
    }

    /**
     * 保存查询结果到数据库
     */
    public void save(YouDaoResult youDaoResult) {
        SearchedWord searchedWord = new SearchedWord();
        searchedWord.setQuery(query);
        Map<String, Object> map = new HashMap<>();
        map.put("query", query);
        try {
            List<SearchedWord> searchedWords = dao.queryForMap(map);
            if (searchedWords == null || searchedWords.size() == 0) {
                if (youDaoResult.getBasic() != null && youDaoResult.getBasic().getExplains() !=
                        null) {
                    searchedWord.setTranslation(youDaoResult.getBasic().getExplains().get(0)
                            .toString());
                }
                dao.add(searchedWord);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取输入的历史数据
     */
    public List<SearchedWord> getSearchWords() {
        try {
            return dao.queryAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        menu.add(1, MENU_DELETE, 1, "删除");
        menu.add(1, MENU_DELETE_ALL, 2, "全部删除");
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        switch (item.getItemId()) {
            case MENU_DELETE:
                try {
                    dao.delete(searchWords.get(position));
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                refresh();
                break;
            case MENU_DELETE_ALL:
                try {
                    dao.deleteAll(searchWords);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                refresh();
                break;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * 刷新历史数据
     */
    public void refresh() {
        List<SearchedWord> data = getSearchWords();
        if(searchWords != null) {
            searchWords.clear();
            searchWords.addAll(data);
            adapter.notifyDataSetChanged();
        }
    }

    public void refreshByQuery() {
        new Thread() {
            @Override
            public void run() {
                List<DBWord> data = dbDao.queryByMeanning(query);
                Message.obtain(handler, QUERY_SUCCEED, data).sendToTarget();
            }
        }.start();
    }

    public void listViewVisible(boolean flag) {
        if (flag) {
            listView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            rlCenter.setVisibility(View.GONE);
        } else {
            listView.setVisibility(View.GONE);
            rlCenter.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        refresh();
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
