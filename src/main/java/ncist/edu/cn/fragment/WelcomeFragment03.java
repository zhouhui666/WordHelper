package ncist.edu.cn.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ncist.edu.cn.R;
import ncist.edu.cn.activity.LoginActivity;
import ncist.edu.cn.utils.Constant;
import ncist.edu.cn.utils.SharedPreferencesUtils;

public class WelcomeFragment03 extends Fragment implements View.OnClickListener {

    private Button btnStart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_welcome_03_layout, null);
        btnStart = (Button) view.findViewById(R.id.welcome_btn_start);
        btnStart.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        SharedPreferencesUtils.putBoolean(getActivity(), Constant.IS_FIRST_OPEN, false);
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}

